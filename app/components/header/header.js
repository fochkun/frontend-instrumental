import './header.less';
import './header.css';

import createMenu from '../../components/menu/menu';
import createLi from '../../components/menu/createLi';

createMainNav();
createCatalogNav();

function createMainNav(){
    var headerRight='<div class="head_menu_rig">Мнение о магазине:' +
        '<li> <a href="#"> Нравится </a></li>' +
        '<li> <a href="#"> Не нравится </a></li>' +
        '<div class="head_menu_rig r"><a href="personal.html"> Личный кабинет </a></div> </div>';

    var liElements=[createLi('O нас','index.html'),
        createLi('Блог','blog.html'),
        createLi('Новости','information.html'),createLi('Документы','#'),
        createLi('Доставка','delivery.html'),createLi('Оплата','basket.html'),
        createLi('Контакты','contacts.html'),headerRight];

    var menu = createMenu(liElements,'main_nav');

    $(".head_menu .container .row#main_menu").append(menu);
    $(".main_nav li:first-child").addClass('active');
}

function createCatalogNav(){
    var catalogElements =[
        createLi('Бензоинструмент','#'),createLi('Электрика','#'),
        createLi('Ручной инструмент','#'),createLi('Электроинструмент','#'),
        createLi('Садовая техника','#'),createLi('Сантехника','#'),
        createLi('Хозяйственные товары','#'),createLi('Расходные материалы','#')
    ];
    var catalogMenu=createMenu(catalogElements,'');
    var navElements=[
        '<li><a href="#"><i class="fa fa-shopping-cart"></i>Каталог товаров</a></li>',
        '<div class="search1">' +
        '<input type="search" name="q" placeholder="Введите название товара (Например: Молоток)" /></div>',
        '<div class="wse_kategor">Все категории</div>',
        '<div class="korzina">15 300 <span class="rub">руб </span></div>'

    ];
    var navPanel=createMenu(navElements,'');
    $('.dws-menu').append(navPanel);
    $('.fa-shopping-cart').append(catalogMenu);
}
