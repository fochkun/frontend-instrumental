const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const sass = require('./webpack/sass');
const less = require('./webpack/less');
const css = require('./webpack/css');
const extractCSS = require('./webpack/css.extract');
const uglifyJS = require('./webpack/js.uglify');
const images = require('./webpack/images');
const fonts = require('./webpack/fonts');
//const createPage = require('./webpack/createPage');
const pages=['index','basket','brend','category','contacts','information','personal','product','delivery','blog'];

const PATHS = {
    source: path.join(__dirname, 'app'),
    build: path.join(__dirname, 'dist'),
};

const common = merge([
    {
        entry: //{
            function(){
                var entryObj={};
                for (var page1 in pages){
                    page1=pages[page1];
                    entryObj[page1]=PATHS.source + '/pages/'+page1+'/'+page1+'.js';
                }
                return entryObj;
            },
            /*'basket': PATHS.source + '/pages/basket/basket.js',
            'brend': PATHS.source + '/pages/brend/brend.js',
            'category': PATHS.source + '/pages/category/category.js',
            'contacts': PATHS.source + '/pages/contacts/contacts.js',
            'information': PATHS.source + '/pages/information/information.js',
            'personal': PATHS.source + '/pages/personal/personal.js',
            'product': PATHS.source + '/pages/product/product.js',
            'index': PATHS.source + '/pages/index/index.js',
            'delivery': PATHS.source + '/pages/delivery/delivery.js',
            'blog': PATHS.source + '/pages/blog/blog.js',
            'ololo': PATHS.source + '/pages/ololo/ololo.js'
        },*/
        output: {
            path: PATHS.build,
            filename: 'js/[name].js'
        },
        plugins: [
            /*new HtmlWebpackPlugin({
                filename: 'basket.html',
                chunks: ['basket', 'common'],
                template: PATHS.source + '/pages/basket/basket.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'brend.html',
                chunks: ['brend', 'common'],
                template: PATHS.source + '/pages/brend/brend.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'category.html',
                chunks: ['category', 'common'],
                template: PATHS.source + '/pages/category/category.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'contacts.html',
                chunks: ['contacts', 'common'],
                template: PATHS.source + '/pages/contacts/contacts.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'information.html',
                chunks: ['information', 'common'],
                template: PATHS.source + '/pages/information/information.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'personal.html',
                chunks: ['personal', 'common'],
                template: PATHS.source + '/pages/personal/personal.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'product.html',
                chunks: ['product', 'common'],
                template: PATHS.source + '/pages/product/product.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                chunks: ['index', 'common'],
                template: PATHS.source + '/pages/index/index.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'delivery.html',
                chunks: ['delivery', 'common'],
                template: PATHS.source + '/pages/delivery/delivery.pug'
            }),
            new HtmlWebpackPlugin({
                filename: 'blog.html',
                chunks: ['blog', 'common'],
                template: PATHS.source + '/pages/blog/blog.pug'
            }),
            createPage('ololo'),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common'
            }),*/
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            })
        ].concat(function(){
            var pagesArr=[];
            for (var page1 in pages){
                page1=pages[page1];
                pagesArr.push(
                    createPage(page1)
                )
                console.log(page1);
            }
            return pagesArr;
        })
    },
    pug(),
    images(),
    fonts()
]);

module.exports = function(env) {
    if (env === 'production'){
        return merge([
            common,
            extractCSS(),
            uglifyJS()
        ])
    }
    if (env === 'development'){
        return merge([
            common,
            devserver(),
            sass(),
            less(),
            css()
        ])
    }
};

function createPage(name){
    return new HtmlWebpackPlugin({
        filename: name+'.html',
        chunks: [name, 'common'],
        template: PATHS.source + '/pages/'+name+'/'+name+'.pug'
    })
}